import pygame
import random

pygame.init()

screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Space Invaders")

running = True
col = 6
row = 3
listEnemies = [[pygame.image.load('enemy.png') for x in range(col)] for y in range(row)]
enemyYShift = 20


#background
bkgImage = pygame.image.load('bkg.jpg')

#player
playerImg = pygame.image.load('rocket.png')
playerX = 370
playerY = 475
playerMoveChange = 0

#enemy movement
enemyGroupX = 60
enemyGroupY = 80
enemyGroupXChange = 0.3
enemyGroupYChange = -0.3

#bullet
#bulletState
# ready = not visable.
# fire = bullet is moving towards top of screen
bulletImg = pygame.image.load('bullet.png')
bulletX = 370
bulletY = 475
bulletMoveChange = .5
bulletState = 'ready'

def playerDraw():
    screen.blit(playerImg, (playerX, playerY))

def enemyDraw():
    global row
    global col
    global enemyGroupY
    global enemyGroupX

    for c in listEnemies:

        for r in c:

            screen.blit(r, (enemyGroupX, enemyGroupY))
            enemyGroupX += enemyGroupXChange




def backGroundDraw():
    screen.blit(bkgImage, (0, 0))

def bulletFire(x,y):
    global bulletState
    global bulletX
    bulletX = x
    bulletState = 'fire'
    screen.blit(bulletImg, (x + 16, y + 10))

while running:
    screen.fill((100, 0, 0))
    backGroundDraw()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif (event.type == pygame.KEYDOWN):
            if (event.key == pygame.K_LEFT):
                playerMoveChange = -0.3
            elif (event.key == pygame.K_RIGHT):
                playerMoveChange = 0.3
            elif (event.key == pygame.K_SPACE and bulletState == 'ready'):
                bulletFire(playerX, bulletY)

        elif (event.type == pygame.KEYUP):
            if (event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT):
                playerMoveChange = 0

    playerX += playerMoveChange
    if playerX < 0:
        playerX = 0
    elif playerX > 720:
        playerX = 720

    # #enemy movement
    # enemyGroupX += enemyGroupXChange
    # if enemyGroupX < 0: #check if hit the left of the screen
    #     enemyGroupXChange = 0.3
    #     enemyGroupY = enemyGroupY + enemyYShift
    # elif enemyGroupX > 500: #check if hit the right of the screen
    #     enemyXChange = -0.3
    #     enemyGroupY = enemyGroupY + enemyYShift

    #bullet movement
    if(bulletY <= 0):
        bulletState = 'ready'
        bulletY = 475
    if(bulletState == 'fire'):
        bulletFire(bulletX, bulletY)
        bulletY -= bulletMoveChange


    playerDraw()
    enemyDraw()
    pygame.display.update()

